document.querySelector('.fixed-button button').addEventListener('click', () => {

    // document.querySelector('.nav_contact').style.display = 'flex';
    // document.querySelector('.nav_accueil').style.display = 'block';
    // document.querySelector('.nav_real').style.display = 'block';

    document.querySelector('.nav_contact').classList.toggle('ouverture_contact')
    document.querySelector('.nav_accueil').classList.toggle('ouverture_accueil')
    document.querySelector('.nav_real').classList.toggle('ouverture_real')

    document.querySelector('.nav_contact').addEventListener('click', () => {
        window.location.href = '../contact.html';
    })
    document.querySelector('.nav_accueil').addEventListener('click', () => {
        window.location.href = '../index.html';
    })
    document.querySelector('.nav_real').addEventListener('click', () => {
        window.location.href = '../realisations.html';
    })
})

