/* eslint-disable no-console */

carrouselInfos = [
  {
    titre: "Pouvoir partagé",
    text: `Nous croyons au pouvoir de la collaboration. L'écoute attentive de chaque membre de notre équipe nous permet de prendre les meilleures décisions possibles, garantissant ainsi des résultats exceptionnels.`,
    src: "./images/imagePouvoirPartage.png"
  },
  {
    titre: "Confiance",
    text: "Nous sommes votre partenaire de confiance, prêt à transformer vos idées en réalité. Contactez-nous aujourd'hui pour démarrer votre prochain projet web avec une équipe qui partage vos valeurs.",
    src: "./images/imageConfiance.png"
  },
  {
    titre: "Empathie",
    text: "La compréhension et la prise en compte des émotions et des besoins des autres sont au cœur de notre approche. Nous nous efforçons de créer des expériences web qui résonnent avec vos utilisateurs.",
    src: "./images/imageEmphatie.png"
  },
  {
    titre: "Innovation",
    text: "L'innovation est le moteur de la créativité et de l'amélioration continue. Nous valorisons et encourageons les idées novatrices, garantissant ainsi des solutions web de pointe pour nos clients.",
    src: "./images/imageInnovation.png"
  },
  {
    titre: "Convivialité",
    text: "L'intégration de nos clients dans le processus de création est essentielle pour répondre à leurs attentes de manière optimale. Nous visons à instaurer un climat de confiance où chaque voix compte.",
    src: "./images/imageConvivialite.png"
  }
];

let index = 0;
let rotation = 0;

const carrousel = document.getElementById('carrousel');
carrousel.querySelector('h2').textContent = carrouselInfos[0].titre;
carrousel.querySelector('p').textContent = carrouselInfos[0].text;
carrousel.querySelector('.image_valeur').setAttribute('src', carrouselInfos[0].src);

function flecheHaut() {
  index = (index + 1) % carrouselInfos.length;
  rotation += 180;
  carrousel.querySelector('#carte').style.transition = '0.8s';
  carrousel.querySelector('#carte').style.transform = `rotateX(${rotation}deg)`;

  carrousel.querySelector('#carte').addEventListener('transitionend', function () {
    carrousel.querySelector('#carte').style.transform = `rotateX(${rotation}deg)`;
    carrousel.querySelector('figure').style.transform = `rotateX(${rotation}deg)`;
  });
  setTimeout(function () {
    carrousel.querySelector('h2').textContent = carrouselInfos[index].titre;
    carrousel.querySelector('p').textContent = carrouselInfos[index].text;
    carrousel.querySelector('.image_valeur').setAttribute('src', carrouselInfos[index].src);
    carrousel.querySelector('.image_valeur').style.transform = `rotateX(${rotation}deg)`;
    carrousel.querySelector('figcaption').style.transform = `rotateX(${rotation}deg)`;
  }, 250);
}

function flecheBas() {
  index = (index - 1 + carrouselInfos.length) % carrouselInfos.length;
  rotation += 180;
  carrousel.querySelector('#carte').style.transition = '0.8s';
  carrousel.querySelector('#carte').style.transform = `rotateX(${rotation}deg)`;


  carrousel.querySelector('#carte').addEventListener('transitionend', function () {
    carrousel.querySelector('#carte').style.transform = `rotateX(${rotation}deg)`;
  });
  setTimeout(function () {
    carrousel.querySelector('h2').textContent = carrouselInfos[index].titre;
    carrousel.querySelector('p').textContent = carrouselInfos[index].text;
    carrousel.querySelector('.image_valeur').setAttribute('src', carrouselInfos[index].src);
    carrousel.querySelector('.image_valeur').style.transform = `rotateX(${rotation}deg)`;
    carrousel.querySelector('figcaption').style.transform = `rotateX(${rotation}deg)`;
  }, 250);
}

document.getElementById('fleche_haut').addEventListener('click', flecheHaut);
document.getElementById('fleche_bas').addEventListener('click', flecheBas);

function reveal() {
  var reveals = document.querySelectorAll(".revealBottom, .revealLeft, .revealYellow");
  for (var i = 0; i < reveals.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals[i].getBoundingClientRect().top;
    var elementVisible = 100;

    if (elementTop < windowHeight - elementVisible) {
      reveals[i].classList.add("active");
    } else {
      reveals[i].classList.remove("active");
    }
  }
}

window.addEventListener("scroll", reveal);
window.onbeforeunload = function () {
  window.scrollTo(0, 0);
}