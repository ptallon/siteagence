const scrollOffset = 50;
const scrollElements = document.querySelectorAll(".scroll, .scrollFromLeft, .scrollFromRight");

const elementInView = (el, offset = 100) => {
    const elementTop = el.getBoundingClientRect().top;
    return (
        elementTop <=
        ((window.innerHeight || document.documentElement.clientHeight) - offset)
    );
};

const displayScrollElement = (el) => {
    el.classList.add('scrolled');
};

const hideScrollElement = (el) => {
    el.classList.remove('scrolled');
};

const handleScrollAnimation = () => {
    scrollElements.forEach((element) => {
        if (elementInView(element, scrollOffset)) {
            displayScrollElement(element);
        } else {
            hideScrollElement(element);
        }
    });
};

window.addEventListener('scroll', () => {
    handleScrollAnimation();
});