<?php

$to = 'contact.wizeus@gmail.com';
if (isset($_POST['email']) && isset($_POST['message'])) {
    $subject = $_POST['email'];
    $message = $_POST['message'];
    $headers =
        'From: webmaster@example.com' . "\r\n" .
        'Reply-To: webmaster@example.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
}

?>
<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <link href="./css/reset.css" rel="stylesheet" />
    <link href="./css/style.css" rel="stylesheet" />
    <link rel="icon" href="./images/logo.ico" type="image/x-icon">
    <link rel="shortcut icon" href="./images/logo.ico" type="image/x-icon">
    <script defer src="js/script.js"></script>
    <script defer src="js/menuMobile.js"></script>
    <script src="https://kit.fontawesome.com/3c7293bb7f.js" crossorigin="anonymous"></script>
    <title>WiZeus - Création web</title>
</head>

<body class="contact">
    <header>
        <nav>
            <img src="images/logo.png" alt="Logo WiZeus">
            <ul>
                <li><a href="index.html">Accueil</a></li>
                <li><a href="realisations.html">Nos réalisations</a></li>
                <li><a href="contact.php" class="actif">Contact</a></li>
            </ul>
        </nav>
    </header>
    <main>
        <svg width="1728" height="401" viewbox="0 0 1728 401" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M-1 286.143L72.2083 276.571C145.417 267 291.833 247.857 438.25 257.429C584.667 267 728.541 326.714 874.958 298C1021.37 269.286 1170.33 231.929 1316.75 241.5C1463.17 251.071 1609.58 286.143 1682.79 343.571L1756 401V-1H1682.79C1609.58 -1 1463.17 -1 1316.75 -1C1170.33 -1 1023.92 -1 877.5 -1C731.083 -1 584.667 -1 438.25 -1C291.833 -1 145.417 -1 72.2083 -1H-1V286.143Z" fill="#121212" />
        </svg>
        <img src="images/Vector.png" alt="Wave" class="wave">
        <form action="" method="post">
            <section>
                <section>
                    <h1>Contactez-nous</h1>
                    <p>Nous appeler : 07 XX XX XX XX</p>
                    <p>Nous rencontrer : 25 av. Albert Einstein, 17000 La Rochelle</p>
                </section>
                <section>
                    <div>
                        <div class="champ_formulaire">
                            <label for="saisieNom">Nom / Prénom :</label>
                            <input id="saisieNom" placeholder="Votre nom / prénom" name="nom" required="" type="text" />
                        </div>
                        <div class="champ_formulaire">
                            <label for="saisieEmail">Adresse e-mail :</label>
                            <input id="saisieEmail" placeholder="Votre adresse e-mail" name="email" required="" type="email" />
                        </div>
                    </div>
                </section>
                <div class="champ_formulaire">
                    <label for="saisieMessage">Message :</label>
                    <textarea placeholder="Décrivez-nous votre projet" id="saisieMessage" name="message" required=""></textarea>
                    <input type="submit" value="Envoyer" />
                </div>
            </section>
        </form>
    </main>
    <footer>
        <h2>WiZeus</h2>
        <nav class="navigation">
            <h3>Navigation</h3>
            <ul>
                <li>
                    <a href="index.html">Accueil</a>
                </li>
                <li>
                    <a href="realisations.html">Nos réalisations</a>
                </li>
                <li>
                    <a href="contact.php" class="actif">Contact</a>
                </li>
                <!-- <li class="copyright">Copyright</li> -->
            </ul>
        </nav>
        <img src="images/logo.png" alt="Logo WiZeus">
        <section class="social">
            <h3>Social</h3>
            <ul class="listeSocial">
                <li>
                    <a href="https://www.linkedin.com/in/alexy-delaporte-b1a040293/">LinkedIn</a>
                    <a href="https://github.com/alexy103">Git</a>
                    <a href="https://adelapor.lpmiaw.univ-lr.fr/portfolio/web/">Portfolio</a>
                    <p>Alexy Delaporte</p>
                </li>
                <li>
                    <a href="https://www.linkedin.com/in/paul-tallon-b13147236/">LinkedIn</a>
                    <a href="https://github.com/paulotuto">Git</a>
                    <a href="#">Portfolio</a>
                    <p>Paul Tallon</p>
                </li>
                <li>
                    <a href="https://www.linkedin.com/in/enzo-savary-536685251/">LinkedIn</a>
                    <a href="https://esavary.lpmiaw.univ-lr.fr/wp_portfolio/">Portfolio</a>
                    <p>Enzo Savary</p>
                </li>
            </ul>

        </section>

        <nav class="fixed-button">
            <div class="nav_contact">
                <i class="fa-regular fa-envelope"></i>
            </div>
            <div class="nav_accueil">
                <i class="fa-solid fa-house"></i>
            </div>
            <div class="nav_real">
                <i class="fa-solid fa-pen-to-square"></i>
            </div>
            <button>
                <i class="fa-solid fa-bars" style="color: #ffd900;"></i>
            </button>
        </nav>
    </footer>
</body>

</html>